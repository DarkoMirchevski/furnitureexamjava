package mk.iwec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FurnitureexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(FurnitureexamApplication.class, args);
	}

}
