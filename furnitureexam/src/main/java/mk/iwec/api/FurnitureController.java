package mk.iwec.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import mk.iwec.model.Furniture;
import mk.iwec.service.FurnitureService;

@AllArgsConstructor
@RestController
@RequestMapping("furniture")
public class FurnitureController {
	
	@Autowired
	private final FurnitureService furnitureService;
	
	@PostMapping
	public void groupFurniture(@RequestBody  List<Furniture> list) {
		this.furnitureService.groupFurniture(list);
	}
	@GetMapping
	public String getMostExpens() {
		return this.furnitureService.getMostExpens();
	}
	
}
