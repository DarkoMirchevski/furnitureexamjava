package mk.iwec.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import lombok.AllArgsConstructor;
import lombok.Data;

import mk.iwec.model.Furniture;

@Data
@AllArgsConstructor
@Repository
public class FurnitureRepositoryImpl implements FurnitureRepository {

	private List<Furniture> furnitureList;

	@Override
	public void groupFurniture(List<Furniture> list) {

		furnitureList.addAll(list);
		checker(furnitureList);

	}

	@Override
	public String getMostExpens() {
		int cup = 0;
		int chair = 0;
		int table = 0;
		for (Furniture f : furnitureList) {
			if (f.getType().equals("cup")) {
				cup += f.getWood() * 9;
			}
			if (f.getType().equals("chair")) {
				chair += f.getWood() * 9 + f.getGlass() * 11;

			}
			if (f.getType().equals("table")) {
				table += f.getWood() * 9 + f.getGlass() * 11 + f.getPlastic() * 4;
			}
		}
		return biggestNumber(cup, chair, table);
	}

	private void checker(List<Furniture> lista) {
		for (Furniture f : lista) {
			if (f.getType().equals("cupboard") && f.getWood() == 0 || f.getGlass() != 0 || f.getPlastic() != 0) {
				lista.remove(f);

			}
			if (f.getType().equals("chair") && f.getWood() == 0 || f.getGlass() == 0 || f.getPlastic() != 0) {
				lista.remove(f);
			}
			if (f.getType().equals("table") && f.getWood() == 0 || f.getGlass() == 0 || f.getPlastic() == 0) {
				lista.remove(f);
			}
		}
	}

	private String biggestNumber(int cup, int chair, int table) {
		if (cup > chair && cup > table) {
			return "cupboard";
		} else if (chair > table) {
			return "chair";
		} else {
			return "table";
		}
	}

}
