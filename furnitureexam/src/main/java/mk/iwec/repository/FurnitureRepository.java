package mk.iwec.repository;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import mk.iwec.model.Furniture;
@Repository
public interface FurnitureRepository {
	
	public void groupFurniture(List<Furniture> list);
	
	public String getMostExpens();

}
