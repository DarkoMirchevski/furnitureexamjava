package mk.iwec.service;

import java.util.List;

import org.springframework.stereotype.Service;

import mk.iwec.model.Furniture;
@Service
public interface FurnitureService {
	
	public void groupFurniture(List<Furniture> list);

	public String getMostExpens();
}
