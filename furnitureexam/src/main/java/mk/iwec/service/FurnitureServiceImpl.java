package mk.iwec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import mk.iwec.model.Furniture;
import mk.iwec.repository.FurnitureRepository;

@Service
@AllArgsConstructor
public class FurnitureServiceImpl implements FurnitureService {

	@Autowired
	private final FurnitureRepository furnitureRepo;

	@Override
	public void groupFurniture(List<Furniture> list) {
		this.furnitureRepo.groupFurniture(list);

	}

	@Override
	public String getMostExpens() {
		return this.furnitureRepo.getMostExpens();
	}

}
