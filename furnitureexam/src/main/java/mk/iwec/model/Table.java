package mk.iwec.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Table implements Furniture {
	
	private String type;
	private Integer wood;
	private Integer glass;
	private Integer plastic;

	@Override
	public Integer getWood() {
		return this.wood;
	}

	@Override
	public Integer getGlass() {
		return this.glass;
	}

	@Override
	public Integer getPlastic() {
		return this.plastic;
	}
	@Override
	public String getType() {
		return this.type;
	}

}
