package mk.iwec.model;

public interface Furniture {
	
	public String getType();
	
	public Integer getWood();
	
	public Integer getGlass();
	
	public Integer getPlastic();
}
